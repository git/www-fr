# French translation of http://www.gnu.org/server/takeaction.html
# Copyright (C) 2005 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Cédric Corazza <cedric.corazza AT wanadoo.fr>, 2005, 2008.
# Denis Barbier <bouzim AT gmail.com>, 2011.
# Thérèse Godefroy <godef.th AT free.fr>, 2011, 2012, 2013, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: takeaction.html\n"
"POT-Creation-Date: 2025-01-20 10:26+0000\n"
"PO-Revision-Date: 2025-01-20 11:49+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Take Action - GNU Project - Free Software Foundation"
msgstr "Agir - Projet GNU - Free Software Foundation"

#. type: Content of: <style>
#, no-wrap
msgid ""
"#content h4 + p { padding-left: 2em; margin-bottom: 0.5em;}\n"
"#content h4 { font-size: 1.1em !important; padding-left: 1em; }\n"
msgstr ""
"#content h4 + p { padding-left: 2em; margin-bottom: 0.5em;}\n"
"#content h4 { font-size: 1.1em !important; padding-left: 1em; }\n"

#. type: Content of: <div><h2>
msgid "Take Action"
msgstr "Agir"

#. type: Content of: <div><p>
msgid ""
"Items on this page for: <a href=\"#act\">Activists</a> - <a href=\"#dev"
"\">Developers</a> - <a href=\"#sys\">Sysadmins</a> - <a href=\"#web"
"\">Webmasters</a> - <a href=\"#xlate\">Translators</a>."
msgstr ""
"Actions pour : <a href=\"#act\">activistes</a> – <a href=\"#dev"
"\">développeurs</a> – <a href=\"#sys\">administrateurs système</a> – <a href="
"\"#web\">webmestres</a> – <a href=\"#xlate\">traducteurs</a>."

#. type: Content of: <div><h3>
msgid "For Free Software Activists&hellip;"
msgstr "Pour les militants du logiciel libre&hellip;"

#. type: Content of: <div><h4>
msgid "Support current FSF campaigns"
msgstr "Soutenez les campagnes actuelles de la FSF"

#. type: Content of: <div><p>
msgid ""
"Please support the <a href=\"https://www.fsf.org/campaigns\">FSF campaigns</"
"a>, currently including: <a href=\"https://www.fsf.org/campaigns/drm.html"
"\">against DRM</a>, <a href=\"https://www.fsf.org/windows\">Upgrade from "
"Windows</a>, <a href=\"https://www.fsf.org/campaigns/playogg/\">PlayOGG</a>, "
"<a href=\"https://endsoftwarepatents.org/\">against software patents</a>, <a "
"href=\"https://www.fsf.org/campaigns/opendocument/\">OpenDocument</a>, <a "
"href=\"https://libreboot.org/\">free BIOS</a>, and <a href=\"https://www.fsf."
"org/campaigns/acta/\">against ACTA</a>.  The overall community site for the "
"global movement for free software is <a href=\"https://libreplanet.org/wiki/"
"Main_Page\">LibrePlanet</a>."
msgstr ""
"Merci de soutenir les <a href=\"https://www.fsf.org/campaigns\">campagnes de "
"la FSF</a>, qui sont actuellement : <a href=\"https://www.fsf.org/campaigns/"
"drm.html\">Contre les DRM</a>, <a href=\"https://www.fsf.org/windows"
"\">Remplacez Windows</a>, <a href=\"https://www.fsf.org/campaigns/playogg/"
"\">PlayOgg<cite/></a>, <a href=\"https://endsoftwarepatents.org/\">Contre "
"les brevets logiciels</a>, <a href=\"https://www.fsf.org/campaigns/"
"opendocument/\">OpenDocument</a>, <a href=\"https://libreboot.org/\">Libérez "
"le BIOS</a> et <a href=\"https://www.fsf.org/campaigns/acta/\">Contre ACTA</"
"a>. Le site de toute la communauté du mouvement mondial pour le logiciel "
"libre est <a href=\"https://libreplanet.org/wiki/Main_Page\">LibrePlanet</a>."

#. type: Content of: <div><h4>
msgid "Help fight software patents"
msgstr "Aidez à combattre les brevets logiciels"

#. type: Content of: <div><p>
msgid ""
"Please help <a href=\"https://endsoftwarepatents.org/\">end software "
"patents</a> worldwide.  Please also support <a href=\"https://www.unitary-"
"patent.eu/\">anti-software patent efforts in Europe</a> specifically.  Watch "
"and share the movie <a href=\"http://patentabsurdity.com\"><cite>Patent "
"Absurdity</cite></a>."
msgstr ""
"Aidez, s'il vous plaît, à <a href=\"https://endsoftwarepatents.org/"
"\">arrêter les brevets logiciels</a> dans le monde. Merci de soutenir aussi, "
"en particulier, <a href=\"https://www.unitary-patent.eu/fr.html\">les "
"efforts contre les brevets logiciels en Europe</a>. Regardez et partagez le "
"film <a href=\"http://patentabsurdity.com\"><cite>Patent Absurdity</cite></"
"a> (L'absurdité des brevets)."

#. type: Content of: <div><h4>
msgid "Call on WIPO to change its name and mission"
msgstr "Appelez l'OMPI à changer de nom et de mission"

#. type: Content of: <div><p>
msgid ""
"Please <a href=\"https://fsfe.org/activities/wipo/wiwo.html\">support this "
"declaration</a> calling on WIPO to change its name and mission."
msgstr ""
"Merci de <a href=\"https://fsfe.org/activities/wipo/wiwo.html\">soutenir "
"cette déclaration</a> qui appelle l'OMPI à changer de nom et de mission."

#. type: Content of: <div><h4>
msgid "Enhance the Free Software Directory"
msgstr "Améliorez le répertoire du logiciel libre"

#. type: Content of: <div><p>
msgid ""
"Contribute to the <a href=\"https://directory.fsf.org/wiki/Main_Page\">Free "
"Software Directory</a>: submit new packages, corrections to existing "
"entries, and <a href=\"https://directory.fsf.org/wiki/"
"Free_Software_Directory:Participate\">helping with maintenance</a>."
msgstr ""
"Contribuez au <a href=\"https://directory.fsf.org/wiki/Main_Page"
"\">répertoire du logiciel libre</a> : proposez de nouveaux programmes, des "
"corrections aux données existantes, et <a href=\"https://directory.fsf.org/"
"wiki/Free_Software_Directory:Participate\">aidez à la maintenance</a>."

#. type: Content of: <div><h4>
msgid ""
"Start a GNU/Linux User Group in your area, and send us your group's "
"information"
msgstr ""
"Montez un groupe d'utilisateurs GNU/Linux dans votre région et envoyez-nous "
"les informations sur votre groupe"

#. type: Content of: <div><p>
msgid ""
"For more information: <a href=\"https://libreplanet.org/wiki/Group_list"
"\">GNU Users Groups</a>."
msgstr ""
"Pour plus d'informations, visitez notre page sur les <a href=\"https://"
"libreplanet.org/wiki/Group_list\">groupes d'utilisateurs GNU</a>."

#. type: Content of: <div><h3>
msgid "For Software Developers&hellip;"
msgstr "Pour les développeurs de logiciels&hellip;"

#. type: Content of: <div><h4>
msgid "Contribute to high priority free software projects"
msgstr "Contribuez à des projets de logiciels libres hautement prioritaires"

#. type: Content of: <div><p>
msgid ""
"As listed on the FSF's <a href=\"https://www.fsf.org/campaigns/priority-"
"projects/\">high-priority projects web page</a>."
msgstr ""
"Comme indiqué sur la <a href=\"https://www.fsf.org/campaigns/priority-"
"projects/\">page web des projets hautement prioritaires</a> de la FSF."

#. type: Content of: <div><h4>
msgid "Contribute to GNUstep"
msgstr "Contribuez au projet GNUstep"

#. type: Content of: <div><p>
msgid ""
"Please contribute as a user and developer to <a href=\"http://www.gnustep.org"
"\">GNUstep</a>, a free object-oriented framework for application "
"development, and help it achieve the status of a complete and featured "
"desktop environment."
msgstr ""
"<a href=\"http://www.gnustep.org\">GNUstep</a> est un environnement libre "
"orienté objet pour le développement d'applications. Merci de contribuer à ce "
"projet, que ce soit en tant qu'utilisateur ou développeur, pour l'aider à "
"devenir un environnement de bureau complet et fonctionnel."

#. type: Content of: <div><h4>
msgid "Take over an unmaintained GNU package"
msgstr "Reprenez un paquet GNU non maintenu"

#. type: Content of: <div><p>
msgid "These GNU packages are looking for a maintainer:"
msgstr "Ces paquets GNU sont à la recherche d'un repreneur : "

#. type: Content of: <div><p>
msgid ""
"<a href=\"/software/cfengine/\">cfengine</a>, <a href=\"/software/halifax/"
"\">halifax</a>, <a href=\"/software/quickthreads/\">quickthreads</a>, <a "
"href=\"/software/guile-sdl/\">guile-sdl</a>, <a href=\"/software/superopt/"
"\">superopt</a>"
msgstr ""
"<a href=\"/software/cfengine/\">cfengine</a>, <a href=\"/software/halifax/"
"\">halifax</a>, <a href=\"/software/quickthreads/\">quickthreads</a>, <a "
"href=\"/software/guile-sdl/\">guile-sdl</a>, <a href=\"/software/superopt/"
"\">superopt</a>"

#. type: Content of: <div><p>
msgid ".  And these packages are looking for co-maintainers:"
msgstr ". Et ceux-ci sont à la recherche d'un comainteneur :"

#. type: Content of: <div><p>
msgid ""
"<a href=\"/software/aspell/\">aspell</a>, <a href=\"/software/bison/"
"\">bison</a>, <a href=\"/software/gnuae/\">gnuae</a>, <a href=\"/software/"
"gettext/\">gettext</a>, <a href=\"/software/gnubik/\">gnubik</a>, <a href=\"/"
"software/metaexchange/\">metaexchange</a>, <a href=\"/software/powerguru/"
"\">powerguru</a>, <a href=\"/software/xboard/\">xboard</a>"
msgstr ""
"<a href=\"/software/aspell/\">aspell</a>, <a href=\"/software/bison/"
"\">bison</a>, <a href=\"/software/gnuae/\">gnuae</a>, <a href=\"/software/"
"gettext/\">gettext</a>, <a href=\"/software/gnubik/\">gnubik</a>, <a href=\"/"
"software/metaexchange/\">metaexchange</a>, <a href=\"/software/powerguru/"
"\">powerguru</a>, <a href=\"/software/xboard/\">xboard</a>"

#. type: Content of: <div><p>
msgid ""
".  See the package web pages for specific information, and <a href=\"/help/"
"evaluation.html#whatmeans\">this general information about GNU packages and "
"maintenance</a>, and then email <a href=\"mailto:maintainers@gnu.org\">&lt;"
"maintainers@gnu.org&gt;</a> if you have time and interest in taking over one "
"of these projects."
msgstr ""
". Consultez les informations détaillées qui se trouvent sur les pages web de "
"ces paquets, ainsi que les <a href=\"/help/evaluation.html#whatmeans"
"\">informations générales sur les paquets GNU et leur maintenance</a>, puis "
"envoyez un courriel à &lt;<a href=\"mailto:maintainers@gnu.org"
"\">maintainers@gnu.org</a>&gt; si vous avez le temps et l'envie d'adopter un "
"de ces projets."

#. type: Content of: <div><h4>
msgid "Contribute to the development of a GNU package"
msgstr "Contribuez au développement d'un paquet GNU"

#. type: Content of: <div><p>
msgid ""
"See &ldquo;<a href=\"https://savannah.gnu.org/people/\">help wanted</"
"a>&rdquo; requests from the maintainers of many packages."
msgstr ""
"Consultez les <a href=\"https://savannah.gnu.org/people/\">appels à l'aide</"
"a> des mainteneurs de nombreux paquets."

#. type: Content of: <div><h4>
msgid "Make your program a GNU package"
msgstr "Faites de votre programme un paquet GNU"

#. type: Content of: <div><p>
msgid ""
"To make your free software package part of the GNU System, see this <a href="
"\"/help/evaluation.html\">information on starting your application</a>."
msgstr ""
"Si vous voulez intégrer le logiciel libre que vous avez créé au système GNU, "
"reportez-vous à ces <a href=\"/help/evaluation.html\">informations pour "
"initier votre demande</a>."

#. type: Content of: <div><h3>
msgid "For System Administrators&hellip;"
msgstr "Pour les administrateurs système&hellip;"

#. type: Content of: <div><h4>
msgid "Help with administration of Savannah"
msgstr "Aidez à l'administration de Savannah"

#. type: Content of: <div><p>
msgid ""
"Help maintain the <a href=\"https://savannah.gnu.org/\">free software "
"hosting site</a> sponsored by GNU: <a href=\"https://savannah.gnu.org/"
"maintenance/HowToBecomeASavannahHacker/\"> more information</a>."
msgstr ""
"Aidez à maintenir <a href=\"https://savannah.gnu.org/\">le site "
"d'hébergement du logiciel libre</a> parrainé par GNU. <a href=\"https://"
"savannah.gnu.org/maintenance/HowToBecomeASavannahHacker/\">Pour en savoir "
"plus</a>."

#. type: Content of: <div><h3>
msgid "For Webmasters&hellip;"
msgstr "Pour les webmestres&hellip;"

#. type: Content of: <div><h4>
msgid "Join the GNU webmasters team"
msgstr "Rejoignez l'équipe des webmestres de GNU"

#. type: Content of: <div><p>
msgid "<a href=\"/people/webmeisters.html#volunteer\">More information</a>."
msgstr ""
"<a href=\"/people/webmeisters.html#volunteer\">Pour en savoir plus</a>."

#. type: Content of: <div><h3>
msgid "For Translators&hellip;"
msgstr "Pour les traducteurs&hellip;"

#. type: Content of: <div><h4>
msgid "Join a GNU translation team"
msgstr "Rejoignez une équipe de traduction de GNU"

#. type: Content of: <div><p>
msgid ""
"Help maintain and update translations of www.gnu.org: <a href=\"/server/"
"standards/README.translations.html\">more information</a>."
msgstr ""
"Aidez à maintenir et à mettre à jour les traductions de www.gnu.org. <a href="
"\"/server/standards/README.translations.html\">Pour en savoir plus</a>."

#. type: Content of: <div><p>
msgid ""
"Finally, the separate page <a href=\"/help/help.html\">How You Can Help the "
"GNU Project</a> has more details and additional items."
msgstr ""
"Enfin, la page dédiée <a href=\"/help/help.html\">Comment aider le projet "
"GNU</a> contient des informations supplémentaires ainsi que d'autres "
"demandes d'aide."

#. type: Content of: <div><p>
msgid ""
"Action items for: <a href=\"#act\">Activists</a> - <a href=\"#dev"
"\">Developers</a> - <a href=\"#sys\">Sysadmins</a> - <a href=\"#web"
"\">Webmasters</a> - <a href=\"#xlate\">Translators</a>."
msgstr ""
"Actions pour : <a href=\"#act\">activistes</a> – <a href=\"#dev"
"\">développeurs</a> – <a href=\"#sys\">administrateurs système</a> – <a href="
"\"#web\">webmasters</a> – <a href=\"#xlate\">traducteurs</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

# Pas de changement significatif en 2025.
#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 2004-2018, 2020, 2022-2025 Free Software Foundation, Inc."
msgstr ""
"Copyright &copy; 2004-2018, 2020, 2022-2024 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Cette page peut être utilisée suivant les conditions de la licence <a rel="
"\"license\" href=\"http://creativecommons.org/licenses/by-nd/4.0/deed.fr"
"\">Creative Commons attribution, pas de modification, 4.0 internationale "
"(CC BY-ND 4.0)</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Cédric Corazza<br /> Révision : <a href=\"mailto:trad-gnu@april."
"org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"
