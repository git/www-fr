# French translation of http://www.gnu.org/music/music.html
# Copyright (C) 2012 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Aurélien Rivière <aurelien.riv AT gmail.com>, 2012.
# Thérèse Godefroy <godef.th AT free.fr>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: music.html\n"
"POT-Creation-Date: 2023-07-30 08:25+0000\n"
"PO-Revision-Date: 2024-03-03 16:17+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "GNU Music and Songs - GNU Project - Free Software Foundation"
msgstr "Musiques et chants du GNU - Projet GNU - Free Software Foundation"

#. ul.songs { margin-left: 5%; }
#. #hap-bash { width: 30em; margin-top: 2em; }
#. #hap-bash p { color: #334683; font-size: 1.125em; }
#. @media (min-width: 45em) {
#.   #listen { margin: .8em 5%; }
#.   ul.songs.columns { column-count: 2; column-gap: 5%; }
#. }
#. type: Content of: <style>
#, no-wrap
msgid ".reduced-width { width: 40em; }\n"
msgstr " \n"

#. type: Content of: <div><h2>
msgid "GNU Music and Songs"
msgstr "Musiques et chants du GNU"

#. type: Content of: <div><div><a>
msgid "<a href=\"/graphics/listen.html\">"
msgstr "<a href=\"/graphics/listen.html\">"

#. type: Attribute 'alt' of: <div><div><a><img>
msgid "A Listening GNU"
msgstr "Un GNU qui écoute"

#. type: Content of: <div><div>
msgid "</a>"
msgstr "</a>"

#. type: Content of: <div><h3>
msgid "Lyrics and performance"
msgstr "Paroles et interprétation"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/music/free-software-song.html\">The Free Software Song</a>"
msgstr ""
"<a href=\"/music/free-software-song.html\">Chanson du logiciel libre</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/music/free-birthday-song.html\">The Free Birthday Song</a>"
msgstr ""
"<a href=\"/music/free-birthday-song.html\">The Free Birthday Song</a> "
"(Chanson d'anniversaire libre)"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/eternal-flame.html\">God Wrote in Lisp Code</a>"
msgstr ""
"<a href=\"/fun/jokes/eternal-flame.html\">God Wrote in Lisp Code</a> (Dieu "
"écrivait en code Lisp)"

#. type: Content of: <div><ul><li>
msgid "<a href=\"http://infinite-hands.draketo.de\">Infinite Hands</a>"
msgstr ""
"<a href=\"http://infinite-hands.draketo.de\">Infinite Hands</a> (Mains "
"innombrables)"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/music/gnu-is-not-unix-song.html\">GNU's Not Unix (v1.2)</a>"
msgstr "<a href=\"/music/gnu-is-not-unix-song.html\">GNU's Not Unix (v1.2)</a>"

#. type: Content of: <div><h3>
msgid "Lyrics only"
msgstr "Paroles seules"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/music/free-firmware-song.html\">The Free Firmware Song</a>"
msgstr ""
"<a href=\"/music/free-firmware-song.html\">Chanson du micrologiciel libre</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/gnu-song.html\">The GNU Song</a>"
msgstr "<a href=\"/fun/jokes/gnu-song.html\">Chanson du GNU</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/music/till_there_was_gnu.html\">'Till There Was GNU</a>"
msgstr ""
"<a href=\"/music/till_there_was_gnu.html\">Jusqu'au jour où vint GNU</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/music/whole-gnu-world.html\">A Whole GNU World</a>"
msgstr ""
"<a href=\"/music/whole-gnu-world.html\">A Whole GNU World</a> (Tout un monde "
"GNU)"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/only-gnu.html\">Only GNU</a>"
msgstr "<a href=\"/fun/jokes/only-gnu.html\">Only GNU</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/lauper.html\">Gnus Just Wanna Have Fun</a>"
msgstr ""
"<a href=\"/fun/jokes/lauper.html\">Gnus Just Wanna Have Fun</a> (Les gnus "
"veulent juste s'amuser)"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/welcome-to-gnu-emacs.html\">The GNU Emacs Song</a>"
msgstr ""
"<a href=\"/fun/jokes/welcome-to-gnu-emacs.html\">Chanson de GNU Emacs</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/music/gdb-song.html\">The GDB Song</a>"
msgstr "<a href=\"/music/gdb-song.html\">Chanson de GDB</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/music/emacsvsvi.html\">Emacs vs Vi</a>"
msgstr "<a href=\"/music/emacsvsvi.html\">Emacs vs Vi</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/vi.song.html\">Addicted to Vi</a>"
msgstr ""
"<a href=\"/fun/jokes/vi.song.html\">Addicted to Vi</a> (Intoxiqué à vi)"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/vim-songs.html\">Vim, Send Me an Angel!</a>"
msgstr ""
"<a href=\"/fun/jokes/vim-songs.html\">Vim, Send Me an Angel!</a> (Vim, "
"envoie-moi un ange)"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/hap-bash.html\">Happiness Is a Bash Prompt</a>"
msgstr "<a href=\"/fun/jokes/hap-bash.html\">Le bonheur est un prompt Bash</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/hackforfreedom.html\">Hack for Freedom</a>"
msgstr "<a href=\"/fun/jokes/hackforfreedom.html\">Hacker pour la liberté</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/music/blues-song.html\">The Programmer's Blues</a>"
msgstr "<a href=\"/music/blues-song.html\">Le blues du programmeur</a>"

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"/fun/jokes/hackersong.html\">If the Beatles were hackers&hellip;</"
"a>"
msgstr ""
"<a href=\"/fun/jokes/hackersong.html\">Si les Beatles étaient des "
"hackers&hellip;</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/wonderful-code.html\">What a Wonderful Code</a>"
msgstr ""
"<a href=\"/fun/jokes/wonderful-code.html\">What a Wonderful Code</a> (Quel "
"code merveilleux)"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/nobody-owns.html\">Nobody Owns This Song</a>"
msgstr ""
"<a href=\"/fun/jokes/nobody-owns.html\">Personne ne possède cette chanson</a>"

#. type: Content of: <div><ul><li>
msgid "<a href=\"/fun/jokes/filks.html\">Some filks</a>"
msgstr "<a href=\"/fun/jokes/filks.html\">Quelques filks</a>"

#. type: Content of: <div><div><a>
msgid "<a href=\"/fun/jokes/hap-bash.html\">"
msgstr "<a href=\"/fun/jokes/hap-bash.html\">"

#. type: Attribute 'alt' of: <div><div><a><img>
msgid "Photo of Richard Stallman on the Dinnertime Sampler show"
msgstr "Photo de Richard Stallman à l'émission Dinnertime Sampler"

#. type: Content of: <div><div><p>
msgid "Happiness is a Bash prompt"
msgstr "Le bonheur est un prompt Bash"

#. type: Content of: <div><p>
msgid ""
"You can also listen to Richard Stallman's appearances on the <a href="
"\"https://web.archive.org/web/20180822053320/http://web.mit.edu/echemi/www/"
"\"> Dinnertime Sampler</a> show on <a href=\"http://www.wmbr.org/\">WMBR</"
"a>, the MIT Campus Radio Station. In these shows, Richard shares some "
"examples of his eclectic musical tastes with you, the listener."
msgstr ""
"Vous pouvez aussi écouter les interventions de Richard Stallman au <a href="
"\"https://web.archive.org/web/20180822053320/http://web.mit.edu/echemi/www/"
"\"><i>Dinnertime Sampler</i></a>, un programme de variété de <a href="
"\"http://www.wmbr.org/\">WMBR</a>, la radio du campus du MIT. Richard y "
"partage quelques-uns de ses goûts musicaux éclectiques avec vous, l'auditeur."

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"https://web.archive.org/web/20171108234846/http://web.mit.edu/"
"echemi/www/stallman.html\">March 23<sup>rd</sup> 2005</a>"
msgstr ""
"<a href=\"https://web.archive.org/web/20171108234846/http://web.mit.edu/"
"echemi/www/stallman.html\">23 mars 2005</a>"

#. type: Content of: <div><ul><li>
msgid ""
"<a href=\"https://web.archive.org/web/20180925153007/http://web.mit.edu/"
"echemi/www/040324.html\">March 24<sup>th</sup> 2004</a>"
msgstr ""
"<a href=\"https://web.archive.org/web/20180925153007/http://web.mit.edu/"
"echemi/www/040324.html\">24 mars 2004</a>"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 1999, 2001, 2005-2007, 2011, 2014-2016, 2019-2023 Free "
"Software Foundation, Inc."
msgstr ""
"Copyright &copy; 1999, 2001, 2005-2007, 2011, 2014-2016, 2019-2023 Free "
"Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Cette page peut être utilisée suivant les conditions de la licence <a rel="
"\"license\" href=\"http://creativecommons.org/licenses/by-nd/4.0/deed.fr"
"\">Creative Commons attribution, pas de modification, 4.0 internationale "
"(CC BY-ND 4.0)</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Aurélien Rivière<br />Révision : <a href=\"mailto:trad-"
"gnu@april.org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"
