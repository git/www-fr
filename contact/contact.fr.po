# French translation of http://www.gnu.org/contact/contact.html
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Cédric Corazza <cedric.corazza AT wanadoo.fr>, 2009.
# Yann Leprince <yleprince@april.org>, 2011.
# Thérèse Godefroy <godef.th AT free.fr>, 2012, 2013, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: contact.html\n"
"POT-Creation-Date: 2021-08-31 11:26+0000\n"
"PO-Revision-Date: 2024-03-03 16:17+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Contacting the GNU Project - GNU Project - Free Software Foundation"
msgstr "Contacter le projet GNU - Projet GNU - Free Software Foundation"

#. type: Content of: <div><h2>
msgid "Contacting the GNU Project"
msgstr "Contacter le projet GNU"

#. type: Content of: <div><p>
msgid ""
"We strive to put the answers to all possible questions you might have on our "
"<a href=\"/\">web site</a>.  However, sometimes, you might have a question "
"or issue that isn't covered.  This page describes how to contact us in these "
"circumstances."
msgstr ""
"Nous nous efforçons de mettre les réponses à toutes les questions que vous "
"pourriez vous poser sur notre <a href=\"/\">site web</a>. Cependant, il se "
"peut que vous ayez une question ou un problème qui n'y est pas traité. Cette "
"page décrit comment nous contacter dans ces circonstances."

#. type: Content of: <div><p>
msgid ""
"Please keep in mind, though, that time spent answering individual queries "
"reduces the time available to work on writing, documenting, protecting and "
"promoting <a href=\"/philosophy/free-sw.html\">free software</a>.  So, "
"please look to see if your question is addressed on our web site first.  If "
"you can't find the answer, use the information below to figure out what to "
"do."
msgstr ""
"Veuillez cependant garder à l'esprit que le temps que nous passons à "
"répondre aux requêtes individuelles réduit d'autant le temps dont nous "
"disposons pour écrire, documenter, protéger et promouvoir le <a href=\"/"
"philosophy/free-sw.html\">logiciel libre</a>. Aussi, veuillez vérifier "
"auparavant qu'il n'existe pas déjà une réponse à votre question sur notre "
"site. Si vous ne trouvez pas la réponse, consultez l'information ci-dessous "
"pour savoir quoi faire."

#. type: Content of: <div><p>
msgid ""
"If <em>(and only if)</em> you still have questions about the GNU Project or "
"the FSF after exploring these resources, you can write to <a href=\"mailto:"
"gnu@gnu.org\">&lt;gnu@gnu.org&gt;</a>. But don't expect an answer overnight!"
msgstr ""
"Si <em>(et seulement si)</em> vous avez encore des questions sur le projet "
"GNU ou la FSF après avoir exploré ces ressources, vous pouvez écrire à &lt;"
"<a href=\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Mais ne vous attendez "
"pas à recevoir une réponse dès le lendemain !"

#. type: Content of: <div><h3>
msgid "Technical support"
msgstr "Assistance technique"

#. type: Content of: <div><h3>
msgid ""
"<small>(technical information about GNU software or other free software, "
"search for a particular free program, etc.)</small>"
msgstr ""
"<small>(renseignements techniques sur les logiciels GNU ou autres logiciels "
"libres, recherche d'un logiciel libre particulier, etc.)</small>"

#. type: Content of: <div><p>
msgid ""
"<em>Please note that we do not provide technical support ourselves.</em>"
msgstr ""
"<em>Veuillez noter que nous ne fournissons pas nous-mêmes d'assistance "
"technique.</em>"

#. type: Content of: <div><p>
msgid "However, help is at hand:"
msgstr "Voici cependant quelques pistes :"

#. type: Content of: <div><ul><li>
msgid ""
"a general page on <a href=\"/software/gethelp.html\">getting help with GNU "
"software</a>;"
msgstr ""
"une page générale expliquant <a href=\"/software/gethelp.html\">comment "
"obtenir de l'aide sur les logiciels GNU</a> ;"

#. type: Content of: <div><ul><li>
msgid ""
"the home page for each GNU package&mdash;its URL is <code>https://www.gnu."
"org/software/<var>pkgname</var></code>; for example <a href=\"/software/"
"emacs/\"><code>https://www.gnu.org/software/emacs</code></a> for GNU Emacs;"
msgstr ""
"les pages d'accueil des paquets GNU – leur URL est de la forme <code>https://"
"www.gnu.org/software/<var>pkgname</var></code> (par exemple, <a href=\"/"
"software/emacs/\"><code>https://www.gnu.org/software/emacs</code></a> pour "
"GNU Emacs) ;"

#. type: Content of: <div><ul><li>
msgid ""
"our <a href=\"https://lists.gnu.org/\">mailing lists</a>&mdash;there is "
"usually one or more lists for each GNU package;"
msgstr ""
"nos <a href=\"https://lists.gnu.org/\">listes de diffusion</a> (la plupart "
"sont en anglais) – il existe normalement une ou plusieurs listes pour chaque "
"paquet GNU ;"

#. type: Content of: <div><ul><li>
msgid ""
"the <a href=\"https://directory.fsf.org/\">Free Software Directory</a>&mdash;"
"it can help you find both GNU and other free software packages;"
msgstr ""
"le <a href=\"https://directory.fsf.org/\">Répertoire du logiciel libre</a> – "
"il peut vous aider à trouver des logiciels libres, qu'ils fassent ou non "
"partie du projet GNU ;"

#. type: Content of: <div><ul><li>
msgid "our <a href=\"/server/irc-rules.html\">IRC chat rooms</a>;"
msgstr ""
"nos <a href=\"/server/irc-rules.html\">salles de dialogue en ligne IRC</a> ;"

#. type: Content of: <div><ul><li>
msgid ""
"finally, our <a href=\"https://www.fsf.org/resources/service/\">service "
"directory</a>&mdash;it will help you find companies and people in your area "
"who can provide support services, usually for a fee."
msgstr ""
"enfin, l'<a href=\"https://www.fsf.org/resources/service/\">annuaire des "
"services</a> – il vous aidera à trouver dans votre région des entreprises ou "
"des personnes qui pourront vous fournir une assistance, généralement "
"moyennant finances."

#. type: Content of: <div><h3>
msgid "Comments on the GNU website"
msgstr "Commentaires sur le site web de GNU"

#. type: Content of: <div><h3>
msgid "<small>(suggestions, dead links, typos, etc.)</small>"
msgstr "<small>(suggestions, liens orphelins, typos, etc.)</small>"

#. type: Content of: <div><ul><li>
msgid ""
"For general pages, contact the GNU webmasters <a href=\"mailto:"
"webmasters@gnu.org\">&lt;webmasters@gnu.org&gt;</a>."
msgstr ""
"Pour les pages générales, contactez les webmestres de GNU &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#. type: Content of: <div><ul><li>
msgid ""
"For translations of the general pages, contact <a href=\"mailto:web-"
"translators@gnu.org\">&lt;web-translators@gnu.org&gt;</a>."
msgstr ""
"Pour les traductions des pages générales, contactez &lt;<a href=\"mailto:web-"
"translators@gnu.org\">web-translators@gnu.org</a>&gt; (ou &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt; s'il s'agit d'une "
"traduction en français)."

#. type: Content of: <div><ul><li>
msgid ""
"For pages of specific GNU packages (including their translations), <a href="
"\"/software/gethelp.html\">contact the maintainer(s)</a> of the particular "
"package, usually on &lt;bug-<var>pkgname</var>@gnu.org&gt;."
msgstr ""
"Pour les pages d'un paquet GNU particulier (y compris leurs traductions), <a "
"href=\"/software/gethelp.html\">contactez le responsable du paquet concerné</"
"a>, le plus souvent à l'adresse &lt;bug-<var>pkgname</var>@gnu.org&gt;."

#. type: Content of: <div><h3>
msgid "Software development and maintenance"
msgstr "Développement et maintenance de logiciel"

#. type: Content of: <div><dl><dt>
msgid "Ideas for the GNU task list"
msgstr "Idées pour la liste des tâches de GNU"

#. type: Content of: <div><dl><dd>
msgid "See our page on <a href=\"/help/help.html\">how to help GNU</a>."
msgstr ""
"Consultez notre <a href=\"/help/help.html\">guide pour aider le projet GNU</"
"a>."

#. type: Content of: <div><dl><dt>
msgid ""
"Maintenance of <a href=\"/philosophy/categories.html#GNUsoftware\">GNU "
"software</a> and evaluation of prospective <a href=\"/philosophy/categories."
"html#GNUprograms\">GNU programs</a>"
msgstr ""
"Maintenance des <a href=\"/philosophy/categories.html#GNUsoftware"
"\">logiciels GNU</a> et évaluation de <a href=\"/philosophy/categories."
"html#GNUprograms\">programmes GNU</a> potentiels"

#. type: Content of: <div><dl><dd>
msgid "First check these documents:"
msgstr "Consultez d'abord ces documents :"

#. type: Content of: <div><dl><dd><ul><li>
msgid "the <a href=\"/prep/standards/\">GNU Coding Standards</a>,"
msgstr ""
"<a href=\"/prep/standards/\">GNU coding standards</a> (les normes GNU de "
"codage),"

#. type: Content of: <div><dl><dd><ul><li>
msgid "<a href=\"/prep/maintain/\">Information for GNU Maintainers</a>, and"
msgstr ""
"<a href=\"/prep/maintain/\">Information for GNU Maintainers</a> "
"(l'information à l'usage des mainteneurs de GNU),"

#. type: Content of: <div><dl><dd><ul><li>
msgid ""
"the <a href=\"/help/evaluation.html\">GNU software evaluation questionnaire</"
"a>."
msgstr ""
"le <a href=\"/help/evaluation.html\">questionnaire d'évaluation des "
"logiciels GNU</a>."

#. type: Content of: <div><dl><dd>
msgid ""
"If questions remain, email <a href=\"mailto:maintainers@gnu.org\">&lt;"
"maintainers@gnu.org&gt;</a>."
msgstr ""
"Si vous avez encore des questions, écrivez à &lt;<a href=\"mailto:"
"maintainers@gnu.org\">maintainers@gnu.org</a>&gt;."

#. type: Content of: <div><dl><dt>
msgid "Savannah hosting"
msgstr "Hébergement sur Savannah"

#. type: Content of: <div><dl><dd>
msgid ""
"To ask questions about <a href=\"https://savannah.gnu.org\">Savannah</a>, "
"our hosting platform, available for both GNU and non-GNU free software "
"projects, please see the <a href=\"https://savannah.gnu.org/contact.php"
"\">Savannah contact page</a>."
msgstr ""
"Si vos questions concernent <a href=\"https://savannah.gnu.org\">Savannah</"
"a>, notre plateforme d'hébergement pour projets de logiciel libre, GNU ou "
"non, consultez la <a href=\"https://savannah.gnu.org/contact.php\">page de "
"contact de Savannah</a>."

#. type: Content of: <div><h3>
msgid "Accounts"
msgstr "Comptes utilisateurs"

#. type: Content of: <div><dl><dt>
msgid "Accounts on GNU/FSF machines"
msgstr "Comptes utilisateurs sur les machines GNU/FSF"

#. type: Content of: <div><dl><dd>
msgid ""
"If you are <strong>actively</strong> maintaining or working on a GNU "
"project, and need an account on a GNU or FSF machine, see <a href=\"/"
"software/README.accounts.html\">machine access information</a>."
msgstr ""
"Si vous travaillez <strong>activement</strong> sur un projet GNU ou que vous "
"en assurez la maintenance, et que vous avez besoin d'un compte sur une "
"machine de GNU ou de la FSF, voyez ce qui concerne l'<a href=\"/software/"
"README.accounts.html\">accès aux machines</a>."

#. type: Content of: <div><dl><dt>
msgid "Personal GNU email aliases"
msgstr "Alias GNU pour le courriel personnel"

#. type: Content of: <div><dl><dd>
msgid ""
"If you have an email forward from the gnu.org domain, and need it updated, "
"contact <a href= \"mailto:sysadmin@gnu.org\">&lt;sysadmin@gnu.org&gt;</a>."
msgstr ""
"Si vous avez une redirection d'adresse électronique depuis le domaine gnu."
"org et que vous devez la mettre à jour, contactez &lt;<a href= \"mailto:"
"sysadmin@gnu.org\">sysadmin@gnu.org</a>&gt;."

#. type: Content of: <div><h3>
msgid "Security reports"
msgstr "Signalement des problèmes de sécurité"

#. type: Content of: <div><h3>
msgid "<small>for gnu.org or one of its subdomains</small>"
msgstr "<small>concernant gnu.org or l'un de ses sous-domaines</small>"

#. type: Content of: <div><ul><li>
msgid ""
"<strong>If you have <a href=\"https://www.gnupg.org/\">GnuPG</a> setup</"
"strong>, send encrypted email the FSF Executive Director, Deputy Director, "
"Web Developer, and Senior Sysadmins listed on our <a href=\"https://www.fsf."
"org/about/staff-and-board\">Staff and Board page</a>."
msgstr ""
"<strong>Si vous pouvez utiliser <a href=\"https://www.gnupg.org/\">GnuPG</"
"a></strong>, envoyez un message chiffré au directeur exécutif de la FSF, au "
"développeur web et aux <i>Senior Sysadmins</i> qui figurent sur la <a href="
"\"https://www.fsf.org/about/staff-and-board\">liste du personnel et des "
"administrateurs</a>."

#. type: Content of: <div><ul><li>
msgid ""
"<strong>If you don't have GnuPG setup</strong>, write to <a href=\"mailto:"
"sysadmin@gnu.org\">&lt;sysadmin@gnu.org&gt;</a>."
msgstr ""
"<strong>Si vous ne pouvez pas utiliser GnuPG</strong>, écrivez à &lt;<a "
"href= \"mailto:sysadmin@gnu.org\">sysadmin@gnu.org</a>&gt;."

#. type: Content of: <div><h3>
msgid "Licensing questions"
msgstr "Questions concernant les licences"

#. type: Content of: <div><dl><dt>
msgid "Licensing violations"
msgstr "Violation de licence"

#. type: Content of: <div><dl><dd>
msgid ""
"If you want to report a free software license violation that you have found, "
"please read our <a href=\"/licenses/gpl-violation.html\">license violation "
"page</a>, and then contact <a href= \"mailto:license-violation@gnu.org\">&lt;"
"license-violation@gnu.org&gt;</a>."
msgstr ""
"Si vous voulez nous faire part d'une violation de licence que vous avez "
"découverte, veuillez lire notre <a href=\"/licenses/gpl-violation.html"
"\">page sur les violations de licence</a>, puis contacter &lt;<a href="
"\"mailto:license-violation@gnu.org\">license-violation@gnu.org</a>&gt;."

#. type: Content of: <div><dl><dt>
msgid "Free software licensing and copyright"
msgstr "Questions sur les licences libres et le copyright"

#. type: Content of: <div><dl><dd>
msgid "Please check"
msgstr "Consultez d'abord"

#. type: Content of: <div><dl><dd><ul><li>
msgid "our <a href=\"/licenses/gpl-faq.html\">licensing FAQ</a>,"
msgstr "la <a href=\"/licenses/gpl-faq.html\">FAQ sur les licences</a>,"

#. type: Content of: <div><dl><dd><ul><li>
msgid "the <a href=\"/licenses/license-list.html\">license list</a>,"
msgstr ""
"la <a href=\"/licenses/license-list.html\">liste de licences commentées</a>,"

#. type: Content of: <div><dl><dd><ul><li>
msgid ""
"<a href=\"/licenses/copyleft.html\">general copyleft information</a>, and"
msgstr ""
"l'<a href=\"/licenses/copyleft.html\">information générale sur le copyleft</"
"a> et"

#. type: Content of: <div><dl><dd><ul><li>
msgid "<a href=\"/licenses/\">related pages</a>."
msgstr "<a href=\"/licenses/\">autres documents sur les licences</a>."

#. type: Content of: <div><dl><dd><p>
msgid ""
"If questions remain, email <a href=\"mailto:licensing@gnu.org\">&lt;"
"licensing@gnu.org&gt;</a>."
msgstr ""
"Si vous avez encore des questions, contactez &lt;<a href=\"mailto:assign@gnu."
"org\">assign@gnu.org</a>&gt;."

#. type: Content of: <div><dl><dd><p>
msgid ""
"The <a href=\"https://www.fsf.org/licensing/\">FSF Licensing and Compliance "
"Lab</a> also offers <a href=\"https://www.fsf.org/licensing/contact\">paid "
"consulting on free software licensing issues</a>."
msgstr ""
"L'équipe du <a href=\"https://www.fsf.org/licensing/\">FSF Licensing and "
"Compliance Lab</a> (chargée des licences et de leur respect) propose un "
"service payant de <a href=\"https://www.fsf.org/licensing/contact\">conseil "
"en matière de licences libres</a>."

#. type: Content of: <div><dl><dt>
msgid "Copyright assignments to the FSF"
msgstr "Transfert de copyright à la FSF"

#. type: Content of: <div><dl><dd>
msgid ""
"To assign your copyright on a GNU program to the FSF, contact <a href="
"\"mailto:assign@gnu.org\">&lt;assign@gnu.org&gt;</a>."
msgstr ""
"Pour transférer à la FSF votre copyright sur un programme GNU, contactez &lt;"
"<a href=\"mailto:assign@gnu.org\">assign@gnu.org</a>&gt;."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

#. type: Content of: <div><p>
msgid "Copyright &copy; 2007, 2010, 2018, 2021 Free Software Foundation, Inc."
msgstr "Copyright &copy; 2007, 2010, 2018, 2021 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Cette page peut être utilisée suivant les conditions de la licence <a rel="
"\"license\" href=\"http://creativecommons.org/licenses/by-nd/4.0/deed.fr"
"\">Creative Commons attribution, pas de modification, 4.0 internationale "
"(CC BY-ND 4.0)</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Cédric Corazza, Yann Leprince<br />Révision : <a href=\"mailto:"
"trad-gnu@april.org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"
