# French translation of http://www.gnu.org/philosophy/freedom-or-copyright-old.fr
# Copyright (C) 2008 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Pierre-Yves Enderlin, 2008.
# Cédric Corazza <cedric.corazza AT wanadoo.fr>, 2008, 2009.
# Thérèse Godefroy <godef.th AT free.fr>, 2012, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: freedom-or-copyright-old.fr\n"
"POT-Creation-Date: 2021-09-16 16:58+0000\n"
"PO-Revision-Date: 2024-03-03 16:18+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Freedom&mdash;or Copyright? (Old Version)  - GNU Project - Free Software "
"Foundation"
msgstr ""
"La liberté&hellip; ou le copyright ? (ancienne version) - Projet GNU - Free "
"Software Foundation"

#. type: Content of: <div><h2>
msgid "Freedom&mdash;or Copyright? (Old Version)"
msgstr "La liberté&hellip; ou le copyright ? (ancienne version)"

#. type: Content of: <div><address>
msgid "by Richard Stallman"
msgstr "par Richard Stallman"

#. type: Content of: <div><div><p>
msgid ""
"There is an <a href=\"/philosophy/freedom-or-copyright.html\"> updated "
"version</a> of this article."
msgstr ""
"Il existe une <a href=\"/philosophy/freedom-or-copyright.html\">version "
"actualisée</a> de cet article."

#. type: Content of: <div><div><p>
msgid ""
"The brave new world of e-books: no more used book stores, no more lending a "
"book to your friend, no more borrowing one from the public library, no "
"purchasing a book except with a credit card that identifies what you read.  "
"Even reading an e-book without authorization is a crime."
msgstr ""
"Le meilleur des mondes avec les livres électroniques : plus de bouquiniste, "
"plus de prêt de livre à un ami, plus d'emprunt à une bibliothèque publique, "
"plus d'achat sans utiliser une carte de crédit qui permet d'identifier ce "
"que vous lisez. Lire un livre électronique sans autorisation, même cela est "
"un délit."

#. type: Content of: <div><p>
msgid ""
"Once upon a time, in the age of the printing press, an industrial regulation "
"was established for the business of writing and publishing. It was called "
"copyright. Copyright's purpose was to encourage the publication of a "
"diversity of written works. Copyright's method was to make publishers get "
"permission from authors to reprint recent writings."
msgstr ""
"Il était une fois, à l'époque de la presse à imprimer, une nouvelle "
"réglementation industrielle qui s'appliquait au métier de l'écriture et de "
"l'imprimerie. On l'avait appelée le copyright. Le but du copyright était "
"d'encourager la publication d'une variété d'œuvres écrites. Sa méthode était "
"d'obliger les éditeurs à obtenir la permission des auteurs pour réimprimer "
"leurs œuvres récentes."

#. type: Content of: <div><p>
msgid ""
"Ordinary readers had little reason to disapprove, since copyright restricted "
"only publication, not the things a reader could do. If it raised the price "
"of a book a small amount, that was only money. Copyright provided a public "
"benefit, as intended, with little burden on the public. It did its job "
"well&mdash;back then."
msgstr ""
"Le lecteur moyen avait peu de raisons de ne pas être d'accord, dans la "
"mesure où le copyright se limitait à la publication, pas à ce qu'il pouvait "
"faire. Même si le prix s'en trouvait légèrement augmenté, il ne s'agissait "
"que d'argent. Le copyright était bénéfique pour le public, comme prévu, et "
"ne représentait pas pour lui un grand fardeau. Il remplissait bien son rôle –"
" à l'époque."

#. type: Content of: <div><p>
msgid ""
"Then a new way of distributing information came about: computers and "
"networks. The advantage of digital information technology is that it "
"facilitates copying and manipulating information, including software, "
"musical recordings and books. Networks offered the possibility of unlimited "
"access to all sorts of data&mdash;an information utopia."
msgstr ""
"Puis une nouvelle façon de distribuer l'information est apparue : les "
"ordinateurs et les réseaux. La technologie numérique a pour avantage de "
"faciliter la copie et la manipulation de l'information, qu'il s'agisse de "
"logiciels, de musique ou de livres. Les réseaux offrent une possibilité "
"d'accès illimité à toutes sortes de données – une utopie de l'information."

#. type: Content of: <div><p>
msgid ""
"But one obstacle stood in the way: copyright. Readers who made use of their "
"computers to share published information were technically copyright "
"infringers. The world had changed, and what was once an industrial "
"regulation on publishers had become a restriction on the public it was meant "
"to serve."
msgstr ""
"Mais un obstacle barrait la route : le copyright. Les lecteurs qui "
"utilisaient leurs ordinateurs pour partager l'information publiée étaient "
"techniquement des contrevenants au copyright. Le monde avait changé, et la "
"réglementation industrielle de jadis était devenue une restriction pour le "
"public qu'elle était censée servir."

#. type: Content of: <div><p>
msgid ""
"In a democracy, a law that prohibits a popular, natural and useful activity "
"is usually soon relaxed. But the powerful publishers' lobby was determined "
"to prevent the public from taking advantage of the power of their computers, "
"and found copyright a suitable weapon. Under their influence, rather than "
"relaxing copyright to suit the new circumstances, governments made it "
"stricter than ever, imposing harsh penalties on readers caught sharing."
msgstr ""
"Dans une démocratie, une loi qui interdit une activité populaire, naturelle "
"et utile est normalement bien vite assouplie. Mais le puissant lobby des "
"éditeurs était déterminé à empêcher le public de tirer avantage de ses "
"ordinateurs et a trouvé dans le copyright l'arme qu'il fallait. Sous son "
"influence, plutôt que d'assouplir le copyright en fonction des nouvelles "
"circonstances, les gouvernements l'ont rendu plus strict que jamais, "
"imposant de rudes pénalités aux lecteurs pris en train de partager."

#. type: Content of: <div><p>
msgid ""
"But that wasn't the last of it. Computers can be powerful tools of "
"domination when a few people control what other people's computers do. The "
"publishers realized that by forcing people to use specially designated "
"software to watch videos and read e-books, they can gain unprecedented "
"power: they can compel readers to pay, and identify themselves, every time "
"they read a book!"
msgstr ""
"Mais ce n'était pas tout. Les ordinateurs peuvent devenir de puissants "
"outils de domination quand seules quelques personnes contrôlent ce que font "
"les ordinateurs des autres. Les éditeurs ont vite compris qu'en forçant les "
"gens à utiliser des logiciels spécifiques pour regarder des vidéos et lire "
"des livres électroniques, ils pouvaient gagner un pouvoir sans précédent : "
"astreindre les gens à payer et à s'identifier, chaque fois qu'ils liraient "
"un livre !"

#. type: Content of: <div><p>
msgid ""
"That is the publishers' dream, and they prevailed upon the U.S. government "
"to enact the Digital Millennium Copyright Act of 1998. This law gives them "
"total legal power over almost anything a reader might do with an e-book, as "
"long as they publish the book in encrypted form. Even reading the book "
"without authorization is a crime."
msgstr ""
"Le rêve pour les éditeurs&hellip; Ces derniers persuadèrent le gouvernement "
"américain de promulguer la loi de 1998 dite <abbr title=\"Digital Millennium "
"Copyright Act\">DMCA</abbr> <a id=\"TransNote1-rev\" href="
"\"#TransNote1\"><sup>1</sup></a>. Cette loi leur donne un pouvoir juridique "
"absolu sur tout ce que pourrait faire un lecteur avec un livre électronique, "
"tant qu'ils publient le livre sous forme chiffrée. Le lire sans "
"autorisation, même cela est un délit !"

#. type: Content of: <div><p>
msgid ""
"We still have the same old freedoms in using paper books. But if e-books "
"replace printed books, that exception will do little good. With &ldquo;"
"electronic ink,&rdquo; which makes it possible to download new text onto an "
"apparently printed piece of paper, even newspapers could become ephemeral. "
"Imagine: no more used book stores; no more lending a book to your friend; no "
"more borrowing one from the public library&mdash;no more &ldquo;leaks&rdquo; "
"that might give someone a chance to read without paying. (And judging from "
"the ads for Microsoft Reader, no more anonymous purchasing of books either.) "
"This is the world publishers have in mind for us."
msgstr ""
"Nous bénéficions toujours des libertés traditionnelles avec les livres "
"imprimés. Mais si les livres électroniques remplacent un jour les livres "
"imprimés, cette exception sera pratiquement inutile. Avec « l'encre "
"électronique », qui permet de télécharger un nouveau texte sur ce qui "
"pourrait passer pour du papier imprimé, même les journaux deviendraient "
"éphémères. Imaginez : plus de bouquiniste, plus de prêt de livres à un ami, "
"plus d'emprunt à la bibliothèque publique – plus de « fuite » qui "
"permettrait à quelqu'un de lire sans payer (et si l'on en croit les "
"publicités pour Microsoft Reader, plus d'acquisition anonyme de livres non "
"plus). C'est là le monde que les éditeurs ont en tête pour nous."

#. type: Content of: <div><p>
msgid ""
"Why is there so little public debate about these momentous changes? Most "
"citizens have not yet had occasion to come to grips with the political "
"issues raised by this futuristic technology. Besides, the public has been "
"taught that copyright exists to &ldquo;protect&rdquo; the copyright holders, "
"with the implication that the public's interests do not count. (The biased "
"term &ldquo;<a href=\"/philosophy/not-ipr.html\">intellectual property</"
"a>&rdquo; also promotes that view; in addition, it encourages the mistake of "
"trying to treat several laws that are almost totally different&mdash;such as "
"copyright law and patent law&mdash;as if they were a single issue.)"
msgstr ""
"Pourquoi y a-t-il aussi peu de débat public autour de ces changements "
"d'importance ? La plupart des citoyens n'ont pas encore eu l'occasion de "
"s'attaquer aux questions politiques nées de cette technologie futuriste. De "
"plus, on a enseigné au public que le copyright « protège » les détenteurs du "
"copyright, avec comme implication que ses intérêts à lui ne comptent pas (le "
"terme trompeur « <a href=\"/philosophy/not-ipr.html\">propriété "
"intellectuelle</a> » alimente aussi ce point de vue ; de plus, il incite à "
"considérer plusieurs branches du droit presque totalement différentes – le "
"droit du copyright et le droit des brevets, par exemple – comme un seul et "
"même sujet)."

#. type: Content of: <div><p>
msgid ""
"But when the public at large begins to use e-books, and discovers the regime "
"that the publishers have prepared for them, they will begin to resist. "
"Humanity will not accept this yoke forever."
msgstr ""
"Mais quand le grand public commencera à utiliser les livres électroniques et "
"qu'il découvrira le régime que les éditeurs ont concocté pour lui, il "
"commencera à entrer dans la résistance. L'humanité n'acceptera pas ce joug "
"ad vitam æternam."

#. type: Content of: <div><p>
msgid ""
"The publishers would have us believe that suppressive copyright is the only "
"way to keep art alive, but we do not need a War on Copying to encourage a "
"diversity of published works; as the Grateful Dead showed, private copying "
"among fans is not necessarily a problem for artists. (In 2007, Radiohead "
"made millions by inviting fans to copy an album and pay whatever amount they "
"wish; a few years before, Stephen King got hundreds of thousands for an e-"
"book which people could copy.) By legalizing the copying of e-books among "
"friends, we can turn copyright back into the industrial regulation it once "
"was."
msgstr ""
"Les éditeurs aimeraient nous faire croire qu'un copyright répressif est la "
"seule façon de garder l'art en vie, mais nous n'avons pas besoin d'une "
"guerre contre le partage pour favoriser la diversité des œuvres publiées : "
"comme l'ont montré les Grateful Dead, la copie privée parmi les fans n'est "
"pas forcément un problème pour les artistes (en 2007, Radiohead a gagné des "
"millions en invitant ses fans à copier un album et à payer le montant qu'ils "
"voulaient ; quelques années auparavant, Stephen King avait gagné des "
"centaines de milliers de dollars pour un livre électronique que les gens "
"pouvaient copier). En légalisant la copie de livres électroniques entre "
"amis, nous pouvons transformer le copyright en ce qu'il fut, une "
"réglementation industrielle."

#. type: Content of: <div><p>
msgid ""
"For some kinds of writing, we should go even further. For scholarly papers "
"and monographs, everyone should be encouraged to republish them verbatim "
"online; this helps protect the scholarly record while making it more "
"accessible. For textbooks and most reference works, publication of modified "
"versions should be allowed as well, since that encourages improvement."
msgstr ""
"Pour certains types d'écrits, il faut même aller plus loin. Les articles de "
"recherche et les monographies, tout le monde doit être encouragé à les "
"republier en ligne « verbatim » (sans modification). Cela contribue à "
"protéger les travaux universitaires tout en les rendant plus accessibles. "
"Pour ce qui est des manuels et de la plupart des ouvrages de référence, la "
"publication de versions modifiées doit elle aussi être permise, dans la "
"mesure où cela encourage les améliorations."

#. type: Content of: <div><p>
msgid ""
"Eventually, when computer networks provide an easy way to send someone a "
"small amount of money, the whole rationale for restricting verbatim copying "
"will go away. If you like a book, and a box pops up on your computer saying "
"&ldquo;Click here to give the author one dollar,&rdquo; wouldn't you click? "
"Copyright for books and music, as it applies to distributing verbatim "
"unmodified copies, will be entirely obsolete. And not a moment too soon!"
msgstr ""
"À terme, quand les réseaux informatiques permettront de faire transiter de "
"petites sommes d'argent, l'ensemble du raisonnement aboutissant à "
"restreindre la copie verbatim aura fait long feu. Si vous aimez un livre et "
"qu'un menu contextuel vous invite à « cliquer ici pour envoyer un dollar à "
"l'auteur », ne cliqueriez-vous pas ? Le copyright sur les livres et la "
"musique, tel qu'il s'applique aujourd'hui à la distribution de copies "
"verbatim non modifiées, deviendra complètement obsolète. Et ce ne sera pas "
"trop tôt !"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""
"<hr /><b>Note de traduction</b><ol>\n"
"<li><a id=\"TransNote1\" href=\"#TransNote1-rev\" class=\"nounderline"
"\">&#8593;</a> \n"
"Loi sur le copyright du millénaire numérique.</li></ol>"

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

# Pas de changement significatif en 2021.
#. type: Content of: <div><p>
msgid "Copyright &copy; 1999, 2021 Richard Stallman"
msgstr "Copyright &copy; 1999 Richard Stallman"

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Cette page peut être utilisée suivant les conditions de la licence <a rel="
"\"license\" href=\"http://creativecommons.org/licenses/by-nd/4.0/deed.fr"
"\">Creative Commons attribution, pas de modification, 4.0 internationale "
"(CC BY-ND 4.0)</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Pierre-Yves Enderlin<br />Révision : <a href=\"mailto:trad-"
"gnu@april.org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"
