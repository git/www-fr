# French translation of http://www.gnu.org/graphics/behroze/index.html
# Copyright (C) 2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Cédric Corazza, 2007.
# Thérèse Godefroy <godef.th AT free.fr>, 2012, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: index.html\n"
"POT-Creation-Date: 2020-10-06 08:28+0000\n"
"PO-Revision-Date: 2024-03-03 16:17+0100\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid "Behroze Nejaati's GNU buttons - GNU Project - Free Software Foundation"
msgstr "Badges GNU, de Behroze Nejaati - Projet GNU - Free Software Foundation"

#. type: Content of: <h2>
msgid "GNU buttons"
msgstr "Badges GNU"

#. type: Content of: <address>
msgid "by Behroze Nejaati"
msgstr "de Behroze Nejaati"

#. type: Content of: <div><div><a>
msgid "<a href=\"/graphics/behroze/behroze-gnu-button1.png\">"
msgstr "<a href=\"/graphics/behroze/behroze-gnu-button1.png\">"

#. type: Attribute 'alt' of: <div><div><a><img>
msgid "&nbsp;[GNU // We run GNU]&nbsp;"
msgstr " [GNU // « We run GNU »] "

#. type: Content of: <div><div>
msgid "</a>"
msgstr "</a>"

#. type: Content of: <div><div><ul><li>
msgid ""
"JPEG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button1.jpg\">96kB</"
"a>&nbsp;(800x365)"
msgstr ""
"JPEG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button1.jpg\">96 ko</"
"a>&nbsp;(800x365)"

#. type: Content of: <div><div><ul><li>
msgid ""
"PNG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button1-small.png\">20kB</"
"a>&nbsp;(300x137),&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button1.png"
"\">75kB</a>&nbsp;(800x365)"
msgstr ""
"PNG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button1-small.png\">20 ko</"
"a>&nbsp;(300x137),&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button1.png"
"\">75 ko</a>&nbsp;(800x365)"

#. type: Content of: <div><div><ul><li>
msgid ""
"SVG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button1.svg\">64kB</a>"
msgstr ""
"SVG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button1.svg\">64 ko</a>"

#. type: Content of: <div><div><a>
msgid "<a href=\"/graphics/behroze/behroze-gnu-button2.png\">"
msgstr "<a href=\"/graphics/behroze/behroze-gnu-button2.png\">"

#. type: Attribute 'alt' of: <div><div><a><img>
msgid "&nbsp;[GNU // Free as in freedom]&nbsp;"
msgstr " [GNU // « Free as in freedom »] "

#. type: Content of: <div><div><ul><li>
msgid ""
"JPEG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button2.jpg\">100kB</"
"a>&nbsp;(800x365)"
msgstr ""
"JPEG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button2.jpg\">100 ko</"
"a>&nbsp;(800x365)"

#. type: Content of: <div><div><ul><li>
msgid ""
"PNG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button2-small.png\">20kB</"
"a>&nbsp;(300x137),&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button2.png"
"\">77kB</a>&nbsp;(800x365)"
msgstr ""
"PNG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button2-small.png\">20 ko</"
"a>&nbsp;(300x137),&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button2.png"
"\">77 ko</a>&nbsp;(800x365)"

#. type: Content of: <div><div><ul><li>
msgid ""
"SVG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button2.svg\">73kB</a>"
msgstr ""
"SVG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button2.svg\">73 ko</a>"

#. type: Content of: <div><div><a>
msgid "<a href=\"/graphics/behroze/behroze-gnu-button3.png\">"
msgstr "<a href=\"/graphics/behroze/behroze-gnu-button3.png\">"

#. type: Content of: <div><div><ul><li>
msgid ""
"JPEG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button3.jpg\">122kB</"
"a>&nbsp;(600x746)"
msgstr ""
"JPEG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button3.jpg\">122 ko</"
"a>&nbsp;(600x746)"

#. type: Content of: <div><div><ul><li>
msgid ""
"PNG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button3-small.png\">26kB</"
"a> (241x300),&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button3.png"
"\">92kB</a>&nbsp;(600x746)"
msgstr ""
"PNG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button3-small.png\">26 ko</"
"a> (241x300),&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button3.png"
"\">92 ko</a>&nbsp;(600x746)"

#. type: Content of: <div><div><ul><li>
msgid ""
"SVG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button3.svg\">82kB</a>"
msgstr ""
"SVG&nbsp; <a href=\"/graphics/behroze/behroze-gnu-button3.svg\">82 ko</a>"

#. type: Content of: <div><p>
msgid "Copyright &copy; 2006 Behroze Nejaati"
msgstr "Copyright &copy; 2006 Behroze Nejaati"

#. type: Content of: <div><p>
msgid ""
"These images are available under the <a rel=\"license\" href=\"/licenses/old-"
"licenses/gpl-2.0.html\">GNU General Public License</a>, version&nbsp;2 or "
"any later version."
msgstr ""
"Ces images sont disponibles sous la <a rel=\"license\" href=\"/licenses/old-"
"licenses/gpl-2.0.html\">licence publique générale GNU</a>, version 2 ou "
"toute version ultérieure."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr " "

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and submitting translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and submitting "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

#. type: Content of: <div><p>
msgid ""
"<em>Page text:</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."
msgstr ""
"<em>Texte de la page :</em>&nbsp; Copyright &copy; 2019, 2020 Free Software "
"Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"Available under the <a rel=\"license\" href=\"https://creativecommons.org/"
"licenses/by-nd/4.0/\">Creative Commons Attribution-NoDerivatives 4.0 "
"International License</a>."
msgstr ""
"Disponible sous la licence <a rel=\"license\" href=\"https://creativecommons."
"org/licenses/by-nd/4.0/deed.fr\">Creative Commons attribution, pas de "
"modification, 4.0 internationale</a> (CC BY-ND 4.0)."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Cédric Corazza<br /> Révision : <a href=\"mailto:trad-gnu@april."
"org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"
