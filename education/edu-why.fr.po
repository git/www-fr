# French translation of http://www.gnu.org/education/edu-why.html
# Copyright (C) 2012 Free Software Foundation, Inc.
# This file is distributed under the same license as the original article.
# Pierrick L'Ébraly <plebraly AT gmail.com>, 2012.
# Thérèse Godefroy <godef.th AT free.fr>, 2012, 2013, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: edu-why.html\n"
"POT-Creation-Date: 2024-05-14 04:58+0000\n"
"PO-Revision-Date: 2024-05-14 13:10+0200\n"
"Last-Translator: Thérèse Godefroy <godef.th AT free.fr>\n"
"Language-Team: French <trad-gnu@april.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: <title>
msgid ""
"Why Educational Institutions Should Use and Teach Free Software - GNU "
"Project - Free Software Foundation"
msgstr ""
"Pourquoi l'éducation doit se servir de logiciel libre et l'enseigner - "
"Projet GNU - Free Software Foundation"

#. type: Content of: <div><a>
msgid "<a id=\"side-menu-button\" class=\"switch\" href=\"#navlinks\">"
msgstr "<a id=\"side-menu-button\" class=\"switch\" href=\"#navlinks\">"

#. type: Attribute 'title' of: <div><a><img>
msgid "Education Contents"
msgstr "Menu Éducation"

#. type: Attribute 'alt' of: <div><a><img>
msgid "&nbsp;[Education Contents]&nbsp;"
msgstr "&nbsp;[Menu Éducation]&nbsp;"

#. type: Content of: <div>
msgid "</a>"
msgstr "</a>"

#. type: Content of: <div><p><a>
msgid "<a href=\"/\">"
msgstr "<a href=\"/\">"

#. type: Attribute 'title' of: <div><p><a><img>
msgid "GNU Home"
msgstr "Accueil GNU"

#. type: Content of: <div><p>
msgid ""
"</a>&nbsp;/ <a href=\"/education/education.html\">Education</a>&nbsp;/ <a "
"href=\"/education/education.html#indepth\">In&nbsp;Depth</a>&nbsp;/"
msgstr ""
"</a>&nbsp;/ <a href=\"/education/education.html\">Éducation</a>&nbsp;/ <a "
"href=\"/education/education.html#indepth\">Pour approfondir</a>&nbsp;/"

#. type: Content of: <div><h2>
msgid "Why Educational Institutions Should Use and Teach Free Software"
msgstr "Pourquoi l'éducation doit se servir de logiciel libre et l'enseigner"

#. type: Content of: <div><div><p>
msgid ""
"<em>&ldquo;Schools should teach their students to be citizens of a strong, "
"capable, independent and free society.&rdquo;</em>"
msgstr ""
"<em>« Les écoles doivent apprendre à leurs élèves à devenir citoyens d'une "
"société forte, compétente, indépendante et libre. »</em>"

#. type: Content of: <div><div><p>
msgid ""
"These are the main reasons why universities and schools of all levels should "
"use exclusively Free Software."
msgstr ""
"Ce sont les raisons principales pour lesquelles universités et écoles de "
"tous niveaux doivent utiliser exclusivement du logiciel libre."

#. type: Content of: <div><div><h3>
msgid "Sharing"
msgstr "Partage"

#. type: Content of: <div><div><p>
msgid ""
"Schools should teach the value of sharing by setting an example. Free "
"software supports education by allowing the sharing of knowledge and tools:"
msgstr ""
"Les établissements scolaires doivent enseigner les valeurs du partage en "
"montrant l'exemple. Le logiciel libre contribue à la pédagogie, car il "
"permet le partage des savoirs et des outils :"

#. type: Content of: <div><div><ul><li>
msgid ""
"<strong>Knowledge</strong>. Many young students have a talent for "
"programming; they are fascinated with computers and eager to learn how their "
"systems work. With proprietary software, this information is a secret so "
"teachers have no way of making it available to their students. But if it is "
"Free Software, the teacher can explain the basic subject and then hand out "
"the source code for the student to read and learn."
msgstr ""
"<strong>Savoirs</strong>. Beaucoup de jeunes élèves ont un don pour la "
"programmation, ils sont fascinés par les ordinateurs et enthousiastes à "
"l'idée d'apprendre comment leurs systèmes fonctionnent. Avec le logiciel "
"privateur,<a id=\"TransNote1-rev\" href=\"#TransNote1\"><sup>1</sup></a> "
"cette information est un secret, donc les enseignants ne peuvent d'aucune "
"façon la rendre accessible à leurs élèves. Mais s'il s'agit de logiciel "
"libre, le professeur peut expliquer les bases, puis leur donner le code "
"source pour qu'ils le lisent et s'instruisent."

#. type: Content of: <div><div><ul><li>
msgid ""
"<strong>Tools</strong>. Teachers can hand out to students copies of the "
"programs they use in the classroom so that they can use them at home. With "
"Free Software, copying is not only authorized, it is encouraged."
msgstr ""
"<strong>Outils</strong>. Les professeurs peuvent distribuer à leurs élèves "
"des copies des programmes qu'ils utilisent en classe pour qu'ils s'en "
"servent chez eux. Avec le logiciel libre, la copie est non seulement "
"autorisée, mais encouragée."

#. type: Content of: <div><div><h3>
msgid "Social Responsibility"
msgstr "Responsabilité sociale"

#. type: Content of: <div><div><p>
msgid ""
"Computing has become an essential part of everyday life. Digital technology "
"is transforming society very quickly, and schools have an influence on the "
"future of society. Their mission is to get students ready to participate in "
"a free digital society by teaching them the skills to make it easy for them "
"to take control of their own lives.  Software should not be under the power "
"of a software developer who unilaterally makes decisions that nobody else "
"can change. Educational institutions should not allow proprietary software "
"companies to impose their power on the rest of society and its future."
msgstr ""
"L'informatique est devenue une partie essentielle du quotidien. Notre "
"société est en mutation très rapide du fait des technologies numériques, et "
"les écoles ont une influence sur son avenir. Leur mission est de préparer "
"les élèves à jouer leur rôle dans une société numérique libre en leur "
"enseignant les savoir-faire qui leur permettront de prendre facilement le "
"contrôle de leurs propres vies. Le logiciel ne doit pas être aux mains d'un "
"développeur qui prend des décisions unilatérales que personne d'autre ne "
"peut modifier. Les établissements d'enseignement ne doivent pas laisser les "
"entreprises du logiciel privateur peser de toute leur puissance sur le reste "
"de la société et sur son avenir."

#. type: Content of: <div><div><h3>
msgid "Independence"
msgstr "Indépendance"

#. type: Content of: <div><div><p>
msgid ""
"Schools have an ethical responsibility to teach strength, not dependency on "
"a single product or a specific powerful company. Furthermore, by choosing to "
"use Free Software, the school itself gains independence from any commercial "
"interests and it avoids vendor lock-in."
msgstr ""
"Les écoles ont une responsabilité éthique : elles doivent enseigner "
"l'autonomie, pas la dépendance vis-à-vis d'un seul produit ou de telle ou "
"telle puissante entreprise. De plus, en choisissant d'utiliser le logiciel "
"libre, l'école elle-même gagne en indépendance vis-à-vis de tout intérêt "
"commercial et évite l'enfermement par un fournisseur."

#. type: Content of: <div><div><ul><li>
msgid ""
"Proprietary software companies use schools and universities as a springboard "
"to reach users and thus impose their software on society as a whole. They "
"offer discounts, or even gratis copies of their proprietary programs to "
"educational institutions, so that students will learn to use them and become "
"dependent on them. After these students graduate, neither they nor their "
"future employers will be offered discounted copies.  Essentially, what these "
"companies are doing is they are recruiting schools and universities into "
"agents to lead people to permanent lifelong dependency."
msgstr ""
"Les entreprises du logiciel privateur utilisent écoles et universités comme "
"tremplin pour atteindre les utilisateurs et ainsi imposer leurs logiciels à "
"la société dans son ensemble. Elles proposent des réductions, voire des "
"exemplaires gratuits de leurs logiciels privateurs aux établissements "
"d'enseignement, pour que les étudiants apprennent à les utiliser et en "
"deviennent dépendants. Une fois que les étudiants auront leur diplôme, ni "
"eux, ni leurs futurs employeurs ne se verront offrir d'exemplaires au "
"rabais. Ce que font essentiellement ces entreprises, c'est de recruter les "
"écoles et les universités comme démarcheurs pour amener les gens à une "
"dépendance permanente à vie."

#. type: Content of: <div><div><ul><li>
msgid ""
"Free software licenses do not expire, which means that once Free Software is "
"adopted, institutions remain independent from the vendor.  Moreover, Free "
"Software licenses grant users the rights not only to use the software as "
"they wish, to copy it and distribute it, but also to modify it in order to "
"meet their own needs. Therefore, if institutions eventually wish to "
"implement a particular function in a piece of software, they can engage the "
"services of any developer to accomplish the task, independently from the "
"original vendor."
msgstr ""
"Les licences libres n'expirent pas : une fois le logiciel libre adopté, les "
"établissements conservent leur indépendance vis-à-vis du fournisseur. De "
"plus, les licences libres donnent à l'utilisateur le droit, non seulement "
"d'utiliser les logiciels comme il le souhaite, de les copier et de les "
"distribuer, mais aussi de les modifier pour les faire répondre à ses propres "
"besoins. Par conséquent, si un établissement décide de mettre en œuvre une "
"fonction spécifique dans un logiciel, il peut recruter les services de "
"n'importe quel développeur sans avoir à passer par le fournisseur initial."

#. type: Content of: <div><div><h3>
msgid "Learning"
msgstr "Apprendre"

#. type: Content of: <div><div><p>
msgid ""
"When deciding where they will study, more and more students are considering "
"whether a university teaches computer science and software development using "
"Free Software. Free software means that students are free to study how the "
"programs work and to learn how to adapt them for their own needs. Learning "
"about Free Software also helps in studying software development ethics and "
"professional practice."
msgstr ""
"Quand ils choisissent l'université où ils iront étudier, de plus en plus "
"d'élèves tiennent compte du fait qu'elle utilise, ou non, le logiciel libre "
"pour enseigner l'informatique et le développement logiciel. La liberté du "
"logiciel signifie que les élèves sont libres d'étudier la façon dont "
"fonctionnent les programmes et d'apprendre à les adapter à leurs propres "
"besoins. S'instruire au sujet du logiciel libre est aussi un atout pour "
"étudier l'éthique et la pratique professionnelle du développement logiciel."

#. type: Content of: <div><div><h3>
msgid "Saving"
msgstr "Économies"

#. type: Content of: <div><div><p>
msgid ""
"This is an obvious advantage that will appeal immediately to many school "
"administrators, but it is a marginal benefit. The main point of this aspect "
"is that by being authorized to distribute copies of the programs at little "
"or no cost, schools can actually aid families facing financial issues, thus "
"promoting fairness and equal opportunities of learning among students."
msgstr ""
"C'est un avantage évident qui attirera tout de suite de nombreux "
"administrateurs, mais un avantage marginal. Le plus important, c'est qu'en "
"étant autorisées à distribuer des copies des programmes à faible coût ou "
"gratuitement, les écoles sont en mesure d'aider les familles qui ont des "
"difficultés financières et ainsi de promouvoir l'équité et l'égalité d'accès "
"au savoir parmi les élèves."

#. type: Content of: <div><div><h3>
msgid "Quality"
msgstr "Qualité"

#. type: Content of: <div><div><p>
msgid ""
"Stable, secure, and easily installed Free Software <a href= \"https://"
"directory.fsf.org/wiki/Category/Education\">solutions</a> are <a href= \"/"
"software/free-software-for-education.html\">available</a> for education "
"already. In any case, excellence of performance is a secondary benefit; the "
"ultimate goal is freedom for computer users."
msgstr ""
"Des logiciels libres stables, sûrs et facilement installables sont <a href="
"\"/software/free-software-for-education.html\">disponible</a> pour "
"l'éducation dès à présent. De toute façon, l'excellence des performances "
"n'est qu'un bénéfice secondaire, le but ultime étant la liberté pour les "
"utilisateurs de l'informatique."

#. type: Content of: <div><div><p>
msgid "See also:"
msgstr "Voir également :"

#. type: Content of: <div><div><div><p>
msgid ""
"<a href=\"/education/edu-schools.html#content\">Why Schools Should "
"Exclusively Use Free Software</a>"
msgstr ""
"<a href=\"/education/edu-schools.html#content\">Pourquoi les écoles doivent "
"utiliser exclusivement du logiciel libre</a>"

#. TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S NOTES*"
msgstr ""
"<hr /><b>Note de traduction</b><ol>\n"
"<li><a id=\"TransNote1\" href=\"#TransNote1-rev\" class=\"nounderline"
"\">&#8593;</a> \n"
"Autre traduction de <i>proprietary</i> : propriétaire.</li></ol>"

#. type: Content of: <div><div><p>
msgid ""
"Please send general FSF &amp; GNU inquiries to <a href=\"mailto:gnu@gnu.org"
"\">&lt;gnu@gnu.org&gt;</a>.  There are also <a href=\"/contact/\">other ways "
"to contact</a> the FSF.  Broken links and other corrections or suggestions "
"can be sent to <a href=\"mailto:webmasters@gnu.org\">&lt;webmasters@gnu."
"org&gt;</a>."
msgstr ""
"Veuillez envoyer les requêtes concernant la FSF et GNU à &lt;<a href="
"\"mailto:gnu@gnu.org\">gnu@gnu.org</a>&gt;. Il existe aussi <a href=\"/"
"contact/\">d'autres moyens de contacter</a> la FSF. Les liens orphelins et "
"autres corrections ou suggestions peuvent être signalés à &lt;<a href="
"\"mailto:webmasters@gnu.org\">webmasters@gnu.org</a>&gt;."

#.  TRANSLATORS: Ignore the original text in this paragraph,
#.         replace it with the translation of these two:
#.         We work hard and do our best to provide accurate, good quality
#.         translations.  However, we are not exempt from imperfection.
#.         Please send your comments and general suggestions in this regard
#.         to <a href="mailto:web-translators@gnu.org">
#.         &lt;web-translators@gnu.org&gt;</a>.</p>
#.         <p>For information on coordinating and contributing translations of
#.         our web pages, see <a
#.         href="/server/standards/README.translations.html">Translations
#.         README</a>. 
#. type: Content of: <div><div><p>
msgid ""
"Please see the <a href=\"/server/standards/README.translations.html"
"\">Translations README</a> for information on coordinating and contributing "
"translations of this article."
msgstr ""
"Merci d'adresser vos commentaires sur les pages en français à &lt;<a href="
"\"mailto:trad-gnu@april.org\">trad-gnu@april.org</a>&gt;, et sur les "
"traductions en général à &lt;<a href=\"mailto:web-translators@gnu.org\">web-"
"translators@gnu.org</a>&gt;. Si vous souhaitez y contribuer, vous trouverez "
"dans le <a href=\"/server/standards/README.translations.html\">guide de "
"traduction</a> les infos nécessaires."

# Pas de changement significatif depuis 2012.
#. type: Content of: <div><p>
msgid ""
"Copyright &copy; 2011, 2015, 2016, 2020, 2021, 2024 Free Software "
"Foundation, Inc."
msgstr "Copyright &copy; 2011, 2012 Free Software Foundation, Inc."

#. type: Content of: <div><p>
msgid ""
"This page is licensed under a <a rel=\"license\" href=\"http://"
"creativecommons.org/licenses/by-nd/4.0/\">Creative Commons Attribution-"
"NoDerivatives 4.0 International License</a>."
msgstr ""
"Cette page peut être utilisée suivant les conditions de la licence <a rel="
"\"license\" href=\"http://creativecommons.org/licenses/by-nd/4.0/deed.fr"
"\">Creative Commons attribution, pas de modification, 4.0 internationale "
"(CC BY-ND 4.0)</a>."

#. TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.
#. type: Content of: <div><div>
msgid "*GNUN-SLOT: TRANSLATOR'S CREDITS*"
msgstr ""
"Traduction : Pierrick L'Ébraly<br /> Révision : <a href=\"mailto:trad-"
"gnu@april.org\">trad-gnu@april.org</a>"

#.  timestamp start 
#. type: Content of: <div><p>
msgid "Updated:"
msgstr "Dernière mise à jour :"
